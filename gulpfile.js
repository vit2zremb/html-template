var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var imagemin = require('gulp-imagemin');
var gcmq = require('gulp-group-css-media-queries');
var nunjucksRender = require('gulp-nunjucks-render');
var prettify = require('gulp-html-prettify');
var htmlmin = require('gulp-htmlmin');
var browserSync = require('browser-sync').create();
var postcss = require('gulp-postcss');
var flexibility = require('postcss-flexibility');
var useref = require('gulp-useref');


var scssSource = 'app/styles/*.scss';
var cssDest = 'app/styles';

gulp.task('default', ['serve']);

gulp.task('serve', ['css-watch'], function () {
    browserSync.init({
        server: './app'
    })
});

gulp.task('css-watch', ['css'], function () {
    return gulp.watch(scssSource, ['css'])
});

gulp.task('css', function () {
    return gulp.src(scssSource)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        // .pipe(postcss([flexibility]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(cssDest))
        .pipe(browserSync.stream());
});

gulp.task('nunjucks-watch', ['nunjucks'], function () {
    return gulp.watch('app/**/*.+(njk|html)', ['nunjucks']);
});


gulp.task('nunjucks', function () {
    return gulp.src('app/pages/*.+(njk)')
        .pipe(plumber())
        .pipe(nunjucksRender({
            path: ['app/templates']
        }))
        // .pipe(gulp.dest('dist'))
        .pipe(gulp.dest('app'));
});


gulp.task('dist', ['css-dist', 'nunjucks'], function () {
    gulp.src('app/fonts/**/*').pipe(gulp.dest('dist/fonts'));

    gulp.src('app/**/*.+(html)')
    // @todo: fix error
    // .pipe(useref())
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(prettify())
        .pipe(gulp.dest('dist'));

    return gulp.src('app/**/*.+(js|css|ico|gif|jpg|png)')
        .pipe(gulp.dest('dist'));
});

gulp.task('css-dist', function () {
    return gulp.src(scssSource)
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 4 version', '> 0.1%'))
        // .pipe(gcmq())
        .pipe(gulp.dest("dist/styles"))
        .pipe(gulp.dest("app/styles"));
});

gulp.task('images', function () {
    gulp.src('app/img/**')
        .pipe(imagemin({
            optimizationLevel: 5
        }))
        .pipe(gulp.dest('app/img'))
        .pipe(gulp.dest('dist/img'))
});




