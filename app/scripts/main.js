$(document).ready(function () {

    // scroll to section (add 'data-scroll' to link)
    $("[data-scroll]").click(function (e) {
        var topOffset = $(this).data('scroll') || 0;
        var destOffset = Math.max(0, $($(this).attr("href")).offset().top - topOffset);
        $('html, body').animate({scrollTop: destOffset}, "slow");
        e.preventDefault();
    });

    // example of number input format
    $("input[name=phone]").mask("+7 (999) 999-99-99");

    $(".js-ajax-form").each(function () {
        $(this).ajaxForm({
            delegation: true,
            clearForm: true,
            resetForm: true,
            type: 'post',
            beforeSubmit: function () {
                $.fancybox.close();
            },
            success: function () {
                $.fancybox.open({src: "#modalThanks", type: 'inline'});
            },
            error: function () {
                $.fancybox.open({src: "#modalError", type: 'inline'});
            }
        });
    });

    // sticky navbar
    var navbar = $(".nav");
    if (navbar.lenght > 0) {
        var stickedClass = "nav-sticked";
        var navTop = navbar.offset().top;
        $(window).scroll(function () {
            if ($(this).scrollTop() > navTop) {
                navbar.addClass(stickedClass);
            } else {
                navbar.removeClass(stickedClass);
            }
        });
    }

    // tabs
    // todo: refactor (improve) or replace with easytabs
    // [data-tabs]>(([data-tabs-controls]>a*N)+(div>[data-tab-content]*N))
    (function () {
        var activateTab = function (link) {
            var tabToActivate = $(link.attr('href'));
            if (!tabToActivate.hasClass('active')) {
                link.closest('[data-tabs]').find('[data-tab-content]').hide().removeClass('active');
                tabToActivate.fadeIn().addClass('active');
            }
        };
        $('[data-tabs]').each(function (i) {
            var tabLinks = $(this).find('[data-tabs-controls] a');
            if (tabLinks.filter('.active').length != 1) {
                tabLinks.removeClass('active');
                $(tabLinks[0]).addClass('active');
            }
            var activeLink = $(this).find('[data-tabs-controls] a.active');
            activateTab(activeLink);
        });
        $('[data-tabs] [data-tabs-controls] a').click(function (e) {
            $(this).closest('ul').find('a').removeClass('active');
            $(this).addClass('active');
            activateTab($(this));
            e.preventDefault();
        })
    })();

    // todo: test and fix
    // flexibility(document.documentElement);
});
